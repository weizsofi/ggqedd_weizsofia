﻿using NUnit.Framework;
using System;
using ELTEBeadandoFeladat.MainPrograms;

namespace ELTEBeadandoFeladat.Tests
{
    [TestFixture]
    public class DemeritPointsCalculatorTests
    {
        [TestCase(50, 0)]
        [TestCase(60, 1)]
        [TestCase(70, 2)]
        [TestCase(80, 3)]
        [TestCase(90, 4)]
        [TestCase(100, 5)]
        [TestCase(110, 6)]
        [TestCase(120, 7)]
        [TestCase(130, 8)]
        [TestCase(140, 9)]
        [TestCase(150, 10)]
        [TestCase(160, 11)]
        [TestCase(170, 12)]
        [TestCase(180, 13)]
        public void Calculate_With_LimitValue(int speed, int point)
        {
            int actual = DemeritPointsCalculator.CalculateDemeritPoints(speed);
            Assert.AreEqual(point, actual);
        }

        [TestCase(45, 0)]
        [TestCase(55, 0)]
        [TestCase(65, 1)]
        [TestCase(75, 2)]
        [TestCase(85, 3)]
        [TestCase(95, 4)]
        [TestCase(105, 5)]
        [TestCase(115, 6)]
        [TestCase(125, 7)]
        [TestCase(135, 8)]
        [TestCase(145, 9)]
        [TestCase(155, 10)]
        [TestCase(165, 11)]
        [TestCase(175, 12)]
        public void Calculate_With_PartitionValue(int speed, int point)
        {
            int actual = DemeritPointsCalculator.CalculateDemeritPoints(speed);
            Assert.AreEqual(point, actual);
        }

        [TestCase(-1)]
        [TestCase(181)]
        public void Calculate_With_InvalidValue(int speed)
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => DemeritPointsCalculator.CalculateDemeritPoints(speed));
        }

    }
}
