﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using ELTEBeadandoFeladat.MainPrograms;
using Moq;
using System.Collections;

namespace ELTEBeadandoFeladat.Tests
{
    [TestFixture]
    class MathTests
    {
        [TestCase(1, 2, 3)]
        [TestCase(5, 6, 11)]
        [TestCase(10, 14, 24)]
        [TestCase(0, 0, 0)]
        public void Add_SimpleValues_Calculate(int a, int b, int expected)
        {
            int actual = TestObject.Add(a,b);
            Assert.AreEqual(expected, actual);
        }

        [TestCase(1, -2, -1)]
        [TestCase(-5, 6, 1)]
        [TestCase(-10, -14, -24)]
        [TestCase(0, -2, -2)]
        public void Add_NegativValues_Calculate(int a, int b, int expected)
        {
            int actual = TestObject.Add(a, b);
            Assert.AreEqual(expected, actual);
        }

        [TestCase(20, 12, 20)]
        [TestCase(50, 52, 52)]
        [TestCase(10, 0, 10)]
        public void ReturnMax_SimpleValues(int a, int b, int expected)
        {
            int actual = TestObject.Max(a, b);
            Assert.AreEqual(expected, actual);
        }

        [TestCase(-20, -12, -12)]
        [TestCase(-50, -52, -50)]
        [TestCase(-10, 0, 0)]
        public void ReturnMax_NegativValues(int a, int b, int expected)
        {
            int actual = TestObject.Max(a, b);
            Assert.AreEqual(expected, actual);
        }

        [TestCase(6)]
        [TestCase(67)]
        [TestCase(80)]
        [TestCase(0)]
        public void GetOddNumbers_Test_SimpleValue(int limit)
        {
            List<int> expected = new List<int>();
            for (var i = 0; i <= limit; i++)
            {
                if (i % 2 != 0)
                {
                    expected.Add(i);
                }
            }

            int j = 0;
            foreach (int oddNumber in TestObject.GetOddNumbers(limit))
            {
                Assert.AreEqual(expected[j], oddNumber);
                j++;
            }
         
        }

        private MainPrograms.Math math;

        public MainPrograms.Math TestObject
        {
            get
            {
                if (math == null)
                {
                    math = new MainPrograms.Math();
                }
                return math;
            }
        }
    }
}
