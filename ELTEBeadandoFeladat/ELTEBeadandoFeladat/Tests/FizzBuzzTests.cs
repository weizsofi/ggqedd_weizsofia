﻿using ELTEBeadandoFeladat.MainPrograms;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace ELTEBeadandoFeladat.Tests
{
    [TestFixture]
    public class FizzBuzzTests
    {
        [TestCase("1", 1)]
        [TestCase("13", 13)]
        [TestCase("19", 19)]
        [TestCase("68", 68)]
        public void NotDivisible_By_3_And_5(string output, int input)
        {
            Assert.AreEqual(output, FizzBuzz.GetOutput(input));
        }

        [TestCase("FizzBuzz", 15)]
        [TestCase("FizzBuzz", 30)]
        [TestCase("FizzBuzz", 150)]
        [TestCase("FizzBuzz", 0)]
        public void Divisible_By_15(string output, int input)
        {
            Assert.AreEqual(output, FizzBuzz.GetOutput(input));
        }

        [TestCase("Fizz", 3)]
        [TestCase("Fizz", 9)]
        [TestCase("Fizz", 18)]
        [TestCase("Fizz", 222)]
        public void Divisible_Only_By_3(string output, int input)
        {
            Assert.AreEqual(output, FizzBuzz.GetOutput(input));
        }

        [TestCase("Buzz", 5)]
        [TestCase("Buzz", 20)]
        [TestCase("Buzz", 70)]
        [TestCase("Buzz", 125)]
        public void Divisible_Only_By_5(string output, int input)
        {
            Assert.AreEqual(output, FizzBuzz.GetOutput(input));
        }
       
    }
}
