﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using ELTEBeadandoFeladat.MainPrograms;
using Moq;

namespace ELTEBeadandoFeladat.Tests
{
    [TestFixture]
    class ProductTests
    {
        [Test]
        public void IsGold_Test()
        {
            TestObject.ListPrice = 100;
            var mockedCustomer = new Mock<ICustomer>();
            mockedCustomer.Setup(x => x.IsGold).Returns(true);
            float expected = (float)(TestObject.ListPrice * 0.7f);
            
            float actual = TestObject.GetPrice(mockedCustomer.Object);

            Assert.AreEqual(expected,actual);
        }

        [Test]
        public void IsNotGold_Test()
        {
            TestObject.ListPrice = 100;

            var mockedCustomer = new Mock<ICustomer>();
            mockedCustomer.Object.IsGold = false;

            float expected = TestObject.ListPrice;

            float actual = TestObject.GetPrice(mockedCustomer.Object);

            Assert.AreEqual(expected, actual);
        }

        private Product product;

        public Product TestObject
        {
            get
            {
                if (product == null)
                {
                    product = new MainPrograms.Product();
                }
                return product;
            }
        }
    }
}
